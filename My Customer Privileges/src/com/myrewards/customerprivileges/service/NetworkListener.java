package com.myrewards.customerprivileges.service;

public interface NetworkListener {
	void onRequestCompleted(String response, String errorString, int eventType);

}
