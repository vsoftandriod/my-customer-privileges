package com.myrewards.customerprivileges.service;

public interface ServiceListener {
	public void onServiceComplete(Object response, int eventType);
}
